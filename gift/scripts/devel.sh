set -e
set -x

stack install :gift --file-watch --exec='./scripts/redo.sh' --ghc-options='-DDEVELOPMENT' $@
