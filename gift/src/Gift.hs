{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}

module Gift where

import Import

import Yesod.Core

import qualified Data.Text as T

import Control.Concurrent.Async
import Control.Lens
import Control.Monad.Logger
import Control.Monad.Trans.Resource (runResourceT)

import qualified Database.Persist.Sqlite as DB
import qualified Network.Wai.Handler.Warp as Warp
import qualified Network.Wai.Middleware.RequestLogger as Wai

import Gift.Data
import Gift.Server

import Gift.Application ()
import Gift.Constants
import Gift.Foundation

import Gift.OptParse

gift :: IO ()
gift = do
    Instructions (DispatchServe ss@ServeSettings {..}) Settings <-
        getInstructions
    runStderrLoggingT $ do
        logInfoN $
            T.unlines
                ["Running Regtool with these settings:", T.pack (ppShow ss)]
        DB.withSqlitePoolInfo
            (DB.mkSqliteConnectionInfo (T.pack $ toFilePath serveSetDbFile) &
             DB.walEnabled .~ False &
             DB.fkEnabled .~ False)
            4 $ \pool -> do
            setupGiftDatabase pool
            liftIO $ do
                app <- makeApp ss pool serveSetAPIHost serveSetAPIPort
                plainApp <- toWaiAppPlain app
                let defMiddles = defaultMiddlewaresNoLogging
                let extraMiddles =
                        if development
                            then Wai.logStdoutDev
                            else Wai.logStdout
                let middle = extraMiddles . defMiddles
                let app_ = middle plainApp
                when development $
                    runStderrLoggingT $
                    logInfoN $
                    mconcat
                        [ "Serving at "
                        , fromMaybe "http://localhost" serveSetWebHost
                        , ":"
                        , T.pack $ show serveSetWebPort
                        ]
                concurrently_
                    (Warp.run serveSetWebPort app_)
                    (runGiftServer serveSetAPIPort pool)

setupGiftDatabase :: DB.ConnectionPool -> LoggingT IO ()
setupGiftDatabase pool = do
    runResourceT $
        flip DB.runSqlPool pool $ do
            logInfoN "Running Migration"
            DB.runMigration migrateAll
            logInfoN "Setting up manual data"
            setupManualData
  where
    setupManualData = do
        forM_ tags $ \t -> do
            mt <- DB.getBy $ UniqueTag $ tagName t
            case mt of
                Nothing -> DB.insert_ t
                _ -> pure ()
        forM_ gifts $ \(n, l, ms) -> do
            mg <- DB.getBy $ UniqueGift l
            case mg of
                Nothing -> do
                    gid <- DB.insert Gift {giftName = n, giftLink = l}
                    forM_ ms $ \(t1, t2) -> do
                        mt1 <-
                            fmap DB.entityKey <$>
                            DB.selectFirst [TagName DB.==. t1] []
                        mt2 <-
                            case t2 of
                                Nothing -> pure (Just Nothing)
                                Just t -> do
                                    ti <-
                                        fmap DB.entityKey <$>
                                        DB.selectFirst [TagName DB.==. t] []
                                    pure $ Just ti
                        case Match gid <$> mt1 <*> mt2 of
                            Nothing -> pure ()
                            Just m -> DB.insert_ m
                _ -> pure ()

tags :: [Tag]
tags =
    [ Tag {tagName = "punny", tagColor = "yellow"}
    , Tag {tagName = "board games", tagColor = "brown"}
    , Tag {tagName = "family", tagColor = "orange"}
    , Tag {tagName = "woman", tagColor = "pink"}
    , Tag {tagName = "man", tagColor = "blue"}
    , Tag {tagName = "biology", tagColor = "green"}
    , Tag {tagName = "nerd", tagColor = "olive"}
    , Tag {tagName = "mathematics", tagColor = "red"}
    , Tag {tagName = "chemistry", tagColor = "violet"}
    , Tag {tagName = "programming", tagColor = "black"}
    , Tag {tagName = "reading", tagColor = "teal"}
    ]

gifts :: [(Text, Text, [(Text, Maybe Text)])]
gifts =
    [ ( "Pun Intended - It's All Pun and Games - Perfect Game for Pun Lovers"
      , "//ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=US&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=norfair-20&marketplace=amazon&region=US&placement=B076MFY2LK&asins=B076MFY2LK&linkId=c0b9bab5896e40237f7df380c9bfb27e&show_border=true&link_opens_in_new_window=true"
      , [ ("punny", Just "board games")
        , ("board games", Nothing)
        , ("family", Nothing)
        ])
    , ( "Dopamine Molecule Necklace Silver Phantom Jewelry"
      , "//ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=US&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=norfair-20&marketplace=amazon&region=US&placement=B014QLWQ7S&asins=B014QLWQ7S&linkId=6c511278be2256d0bbd994c62338b583&show_border=true&link_opens_in_new_window=true"
      , [("biology", Just "woman"), ("chemistry", Just "woman")])
    , ( "Handmade Glass Klein Bottle"
      , "//ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=US&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=norfair-20&marketplace=amazon&region=US&placement=B017UY60MK&asins=B017UY60MK&linkId=deaa441ea49409ccc2f6b345d43799f2&show_border=true&link_opens_in_new_window=true"
      , [("nerd", Nothing), ("mathematics", Nothing)])
    , ( "The Art of Computer Programming, Volumes 1-4A Boxed Set"
      , "//ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=US&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=norfair-20&marketplace=amazon&region=US&placement=0321751043&asins=0321751043&linkId=f41554ca86b25a34f8112c6b4ec250b2&show_border=true&link_opens_in_new_window=true"
      , [("programming", Nothing), ("programming", Just "reading")])
    ]
