{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ViewPatterns #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Gift.Application where

import Yesod.Core

import Gift.Foundation

import Gift.Handler.Home

mkYesodDispatch "App" resourcesApp
