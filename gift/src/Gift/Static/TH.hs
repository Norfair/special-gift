{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE CPP #-}

module Gift.Static.TH
    ( mkFrontendStatic
    ) where

import Data.Maybe
import System.Environment

import Yesod.EmbeddedStatic

import Gift.Constants

import Language.Haskell.TH

mkFrontendStatic :: Q [Dec]
mkFrontendStatic = do
    menv <- runIO $ lookupEnv "FRONT_CODE"
    mkEmbeddedStatic
        development
        "frontEmbeddedStatic"
        [embedFileAt "front.js" $ fromMaybe "../gift-front/elm-stuff/front.js" menv
            , embedFile "static/semantic/dist/semantic.min.css"
            , embedFile "static/semantic/dist/semantic.min.js"
            , embedDirAt
                  "static/semantic/dist/themes/default/assets/fonts"
                  "static/semantic/dist/themes/default/assets/fonts"

        ]
