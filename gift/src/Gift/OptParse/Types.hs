{-# LANGUAGE DeriveGeneric #-}

module Gift.OptParse.Types where

import Import

data Instructions =
    Instructions Dispatch
                 Settings

data Dispatch =
    DispatchServe ServeSettings

data ServeSettings = ServeSettings
    { serveSetWebPort :: Int
    , serveSetWebHost :: Maybe Text
    , serveSetAPIPort :: Int
    , serveSetAPIHost :: Maybe Text
    , serveSetDbFile :: Path Abs File
    , serveSetTracking :: Maybe Text
    , serveSetVerification :: Maybe Text
    } deriving (Show, Eq, Generic)

data Settings =
    Settings
    deriving (Show, Eq, Generic)

data Arguments =
    Arguments Command
              Flags

data Command =
    CommandServe ServeFlags

data ServeFlags = ServeFlags
    { serveFlagWebPort :: Maybe Int
    , serveFlagWebHost :: Maybe Text
    , serveFlagAPIPort :: Maybe Int
    , serveFlagAPIHost :: Maybe Text
    , serveFlagDbFile :: Maybe FilePath
    , serveFlagTracking :: Maybe Text
    , serveFlagVerification :: Maybe Text
    } deriving (Show, Eq, Generic)

data Environment = Environment
    { envWebPort :: Maybe Int
    , envWebHost :: Maybe Text
    , envAPIPort :: Maybe Int
    , envAPIHost :: Maybe Text
    , envDbFile :: Maybe FilePath
    , envTracking :: Maybe Text
    , envVerification :: Maybe Text
    } deriving (Show, Eq, Generic)

data Flags =
    Flags
    deriving (Show, Eq, Generic)
