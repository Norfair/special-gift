module Gift.Widget where

import Data.Default
import Language.Haskell.TH.Syntax (Exp, Q)

import Yesod.Default.Util
    ( WidgetFileSettings
    , widgetFileNoReload
    , widgetFileReload
    )

import Gift.Constants

widgetFile :: String -> Q Exp
widgetFile =
    if development
        then widgetFileReload widgetFileSettings
        else widgetFileNoReload widgetFileSettings

widgetFileSettings :: WidgetFileSettings
widgetFileSettings = def
