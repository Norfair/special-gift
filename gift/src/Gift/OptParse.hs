{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}

module Gift.OptParse
    ( getInstructions
    , Instructions(..)
    , Dispatch(..)
    , Settings(..)
    , ServeSettings(..)
    ) where

import Import

import Data.String
import qualified Data.Text as T
import Text.Read

import System.Environment (getArgs, getEnvironment)

import Options.Applicative

import Gift.OptParse.Types

getInstructions :: IO Instructions
getInstructions = do
    args <- getArguments
    env <- getFullEnvironment
    buildInstructions args env

buildInstructions :: Arguments -> Environment -> IO Instructions
buildInstructions (Arguments cmd Flags) Environment {..} =
    Instructions <$> disp <*> sets
  where
    disp =
        case cmd of
            CommandServe ServeFlags {..} -> do
                dbf <-
                    resolveFile' $
                    fromMaybe "gift.db" $ serveFlagDbFile <|> envDbFile
                pure $
                    DispatchServe
                        ServeSettings
                            { serveSetWebPort =
                                  fromMaybe 3000 $
                                  serveFlagWebPort <|> envWebPort
                            , serveSetWebHost = serveFlagWebHost <|> envWebHost
                            , serveSetAPIPort =
                                  fromMaybe 3001 $
                                  serveFlagAPIPort <|> envAPIPort
                            , serveSetAPIHost = serveFlagAPIHost <|> envAPIHost
                            , serveSetDbFile = dbf
                            , serveSetTracking =
                                  serveFlagTracking <|> envTracking
                            , serveSetVerification =
                                  serveFlagVerification <|> envVerification
                            }
    sets = pure Settings

getArguments :: IO Arguments
getArguments = do
    args <- getArgs
    let result = serveArgumentsParser args
    handleParseResult result

serveArgumentsParser :: [String] -> ParserResult Arguments
serveArgumentsParser =
    execParserPure
        ParserPrefs
            { prefMultiSuffix = ""
            , prefDisambiguate = True
            , prefShowHelpOnError = True
            , prefShowHelpOnEmpty = True
            , prefBacktrack = True
            , prefColumns = 80
            }
        argParser

argParser :: ParserInfo Arguments
argParser = info (helper <*> parseArgs) (fullDesc <> progDesc "Gift")

parseArgs :: Parser Arguments
parseArgs = Arguments <$> parseCommand <*> parseFlags

parseCommand :: Parser Command
parseCommand = hsubparser $ mconcat [command "serve" parseCommandServe]

parseCommandServe :: ParserInfo Command
parseCommandServe = info parser modifier
  where
    parser =
        CommandServe <$>
        (ServeFlags <$>
         option
             (Just <$> auto)
             (mconcat
                  [ long "web-port"
                  , metavar "PORT"
                  , value Nothing
                  , help "the port to serve web requests on"
                  ]) <*>
         option
             (Just . T.pack <$> str)
             (mconcat
                  [ long "web-host"
                  , value Nothing
                  , metavar "HOST"
                  , help "the host to serve web requests on"
                  ]) <*>
         option
             (Just <$> auto)
             (mconcat
                  [ long "api-port"
                  , metavar "PORT"
                  , value Nothing
                  , help "the port to serve api requests on"
                  ]) <*>
         option
             (Just . T.pack <$> str)
             (mconcat
                  [ long "api-host"
                  , value Nothing
                  , metavar "HOST"
                  , help "the host to serve api requests on"
                  ]) <*>
         option
             (Just <$> str)
             (mconcat
                  [ long "database-file"
                  , value Nothing
                  , metavar "FILE"
                  , help "The database file to use"
                  ]) <*>
         option
             (Just . T.pack <$> str)
             (mconcat
                  [ long "analytics-tracking-id"
                  , value Nothing
                  , metavar "TRACKING_ID"
                  , help "The google analytics tracking ID"
                  ]) <*>
         option
             (Just . T.pack <$> str)
             (mconcat
                  [ long "search-console-verification"
                  , value Nothing
                  , metavar "VERIFICATION_TAG"
                  , help
                        "The contents of the google search console verification tag"
                  ]))
    modifier = fullDesc <> progDesc "Serve gift"

parseFlags :: Parser Flags
parseFlags = pure Flags

getFullEnvironment :: IO Environment
getFullEnvironment = do
    env <- getEnvironment
    let v k = fromString <$> lookup k env
    let r :: Read a => String -> Maybe a
        r k = v k >>= readMaybe
    pure
        Environment
            { envWebPort = r "WEB_PORT"
            , envWebHost = v "WEB_HOST"
            , envAPIPort = r "API_PORT"
            , envAPIHost = v "API_HOST"
            , envDbFile = r "PORT"
            , envTracking = v "TRACKING"
            , envVerification = v "SEARCH_CONSOLE_VERIFICATION"
            }
