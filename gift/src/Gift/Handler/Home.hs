{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module Gift.Handler.Home where

import Gift.Handler.Import

import Text.Julius (RawJS(..))

import qualified Data.Text as T

getHomeR :: Handler Html
getHomeR = do
    url <- apiUrl
    defaultLayout $ do
        setTitle "Special Gift"
        addScript $ StaticR front_js
        $(widgetFile "home")

apiUrl :: Handler Text
apiUrl = do
    app <- getYesod
    pure $
        case appApiHost app of
            Nothing -> "http://localhost:" <> T.pack (show (appApiPort app))
            Just h -> "https://" <> h
