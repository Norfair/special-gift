{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}

module Gift.Foundation where

import Import

import Yesod.Core
import Yesod.EmbeddedStatic

import Database.Persist.Sqlite

import Text.Hamlet

import Gift.Constants
import Gift.OptParse.Types
import Gift.Static
import Gift.Widget

data App = App
    { appStatic :: EmbeddedStatic
    , appTracking :: Maybe Text
    , appVerification :: Maybe Text
    , appDBConnectionPool :: ConnectionPool
    , appApiHost :: Maybe Text
    , appApiPort :: Int
    }

mkYesodData "App" $(parseRoutesFile "routes")

instance Yesod App where
    approot = ApprootRelative
    defaultLayout widget = do
        pc <- widgetToPageContent $ $(widgetFile "default-body")
        app <- getYesod
        withUrlRenderer $ do
            let analytics =
                    if development
                        then mempty
                        else mempty
                -- $(hamletFile "templates/analytics.hamlet")
            $(hamletFile "templates/default-page.hamlet")

makeApp :: ServeSettings -> ConnectionPool -> Maybe Text -> Int -> IO App
makeApp ServeSettings {..} pool apiHost apiPort = do
    pure
        App
            { appStatic = frontEmbeddedStatic
            , appTracking = serveSetTracking
            , appVerification = serveSetVerification
            , appDBConnectionPool = pool
            , appApiHost = apiHost
            , appApiPort = apiPort
            }
