module Main exposing (main)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Http
import Json.Decode as Decode


main : Program Flags Model Msg
main =
    programWithFlags
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Flags =
    { apiUrl : String
    }


type Msg
    = GotText (Result Http.Error FrontPage)


type Model
    = Failure Http.Error
    | Loading
    | Success FrontPage


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( Loading
    , Http.send GotText
        (Http.get
            (flags.apiUrl ++ "/front")
            decodeFrontPage
        )
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotText result ->
            case result of
                Ok fullText ->
                    ( Success fullText, Cmd.none )

                Err f ->
                    ( Failure f, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


type alias FrontPage =
    { gifts : List Gift
    }


decodeFrontPage : Decode.Decoder FrontPage
decodeFrontPage =
    Decode.map FrontPage (Decode.field "gifts" (Decode.list decodeGift))


type alias Gift =
    { name : String
    , link : String
    , matches : List Match
    }


decodeGift : Decode.Decoder Gift
decodeGift =
    Decode.map3 Gift
        (Decode.field "name" Decode.string)
        (Decode.field "link" Decode.string)
        (Decode.field "matches" (Decode.list decodeMatch))


type alias Match =
    { tag : Tag
    , tag2 : Maybe Tag
    }


decodeMatch : Decode.Decoder Match
decodeMatch =
    Decode.map2 Match
        (Decode.field "tag" decodeTag)
        (Decode.field "tag2" (Decode.nullable decodeTag))


type alias Tag =
    { name : String
    , color : String
    }


decodeTag : Decode.Decoder Tag
decodeTag =
    Decode.map2 Tag
        (Decode.field "name" Decode.string)
        (Decode.field "color" Decode.string)


view : Model -> Html Msg
view model =
    div [ class "ui main container" ]
        [ div [ class "hidden divider" ] []
        , div [ class "ui very padded segment" ]
            [ div
                [ class "ui center aligned icon header" ]
                [ h1 [] [ text "Find a Special Gift for a Special Person" ]
                , p [] [ text "See our special recommendations below:" ]
                ]
            ]
        , case model of
            Success frontPage ->
                div []
                    (List.map
                        giftSegment
                        frontPage.gifts
                    )

            Loading ->
                text "loading"

            Failure e ->
                case e of
                    Http.BadUrl _ ->
                        text "bad url"

                    Http.Timeout ->
                        text "timeout"

                    Http.NetworkError ->
                        text "network error"

                    Http.BadStatus _ ->
                        text "bad status"

                    Http.BadPayload _ _ ->
                        text "bad payload"
        , div [ class "ui padded segment" ]
            [ h4 [] [ text "About" ]
            , p []
                [ text "Finding a great gift for a special person can be difficult." ]
            , p
                []
                [ text "You may only know what they like and what kind of person they are, but knowing what that kind of person may be happy to receive is a different story entirely." ]
            , p
                []
                [ text "This site aims to help you make your special friend happy." ]
            ]
        ]


giftSegment : Gift -> Html Msg
giftSegment gift =
    div [ class "ui padded segment" ]
        [ div [ class "ui stackable grid" ]
            [ div [ class "three wide column" ]
                [ affiliateLink gift.link ]
            , div [ class "thirteen wide column" ]
                [ h3 [] [ text gift.name ]
                , giftCategoryLabels gift.matches
                ]
            ]
        ]


giftCategoryLabels : List Match -> Html Msg
giftCategoryLabels categories =
    div [ style [ ( "padding", "14px" ) ], class "ui stackable grid" ] (List.map categoryLabel categories)


categoryLabel : Match -> Html Msg
categoryLabel m =
    div [ style [ ( "padding", "2px" ) ] ]
        (let
            t1 =
                m.tag
         in
         case m.tag2 of
            Nothing ->
                [ div [ class "ui mini button", class t1.color ] [ text t1.name ]
                ]

            Just t2 ->
                [ div [ class "ui mini left attached button", class t1.color ] [ text t1.name ]
                , div [ class "ui mini right attached button", class t2.color ] [ text t2.name ]
                ]
        )


affiliateLink : String -> Html Msg
affiliateLink link =
    iframe
        [ style [ ( "width", "120px" ), ( "height", "240px" ) ]
        , attribute "marginwidth" "0"
        , attribute "marginheight" "0"
        , attribute "scrolling" "no"
        , attribute "frameborder" "0"
        , src link
        ]
        []
