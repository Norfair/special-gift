{-# LANGUAGE DataKinds #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeOperators #-}

module Gift.Server where

import Control.Monad.Reader
import Data.List
import Data.Maybe
import Database.Persist.Sqlite as DB
import Network.Wai as Wai
import Network.Wai.Handler.Warp as Warp
import Network.Wai.Middleware.Cors as Cors
import Servant

import Gift.API
import Gift.Data

type GiftHandler = ReaderT Env Handler

data Env = Env
    { envDBConnectionPool :: ConnectionPool
    }

runGiftServer :: Int -> ConnectionPool -> IO ()
runGiftServer port pool =
    Warp.run port $ giftApplication Env {envDBConnectionPool = pool}

giftApplication :: Env -> Wai.Application
giftApplication e =
    simpleCors $
    serve giftAPI $ hoistServer giftAPI (flip runReaderT e) giftServer

giftServer :: ServerT GiftAPI GiftHandler
giftServer = getFrontPageServer :<|> getSearchServer

runDB :: SqlPersistM a -> GiftHandler a
runDB query = do
    pool <- asks envDBConnectionPool
    liftIO $ runSqlPersistMPool query pool

getFrontPageServer :: GiftHandler FrontPage
getFrontPageServer = do
    giftInfos <-
        runDB $ do
            giftEs <- selectList [] [Desc GiftId, LimitTo 10]
            matchEs <- selectList [MatchGift <-. map entityKey giftEs] []
            tagEs <-
                selectList
                    [ TagId <-.
                      (map (matchTag . entityVal) matchEs ++
                       mapMaybe (matchTag2 . entityVal) matchEs)
                    ]
                    []
            pure $
                flip map giftEs $ \(Entity gi Gift {..}) ->
                    GiftInfo
                    { giftInfoName = giftName
                    , giftInfoLink = giftLink
                    , giftInfoMatches =
                          let mkTag i = do
                                  Entity _ Tag {..} <-
                                      find ((== i) . entityKey) tagEs
                                  pure $
                                      TagInfo
                                      { tagInfoName = tagName
                                      , tagInfoColor = tagColor
                                      }
                          in flip mapMaybe matchEs $ \(Entity _ Match {..}) -> do
                                 guard (matchGift == gi)
                                 t1 <- mkTag matchTag
                                 t2 <-
                                     case matchTag2 of
                                         Nothing -> pure Nothing
                                         Just t -> do
                                             ti <- mkTag t
                                             pure $ Just ti
                                 pure $
                                     MatchInfo
                                     {matchInfoTag = t1, matchInfoTag2 = t2}
                    }
    pure FrontPage {frontPageGifts = giftInfos}

getSearchServer :: Maybe [TagQuery] -> GiftHandler SearchResults
getSearchServer = undefined
