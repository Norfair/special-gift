{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeOperators #-}

module Gift.API where

import GHC.Generics (Generic)

import Data.Aeson
import Data.Proxy
import Data.Text (Text)

import Servant.API

import Gift.Data

type GiftAPI = GetFrontPage :<|> GetSearch

giftAPI :: Proxy GiftAPI
giftAPI = Proxy

type GetFrontPage = "front" :> Get '[ JSON] FrontPage

data FrontPage = FrontPage
    { frontPageGifts :: [GiftInfo]
    } deriving (Show, Eq, Generic)

instance FromJSON FrontPage where
    parseJSON = withObject "FrontPage" $ \o -> FrontPage <$> o .: "gifts"

instance ToJSON FrontPage where
    toJSON FrontPage {..} = object ["gifts" .= frontPageGifts]

type GetSearch
     = "search" :> QueryParam "query" [TagQuery] :> Get '[ JSON] SearchResults

data TagQuery
    = ById TagId
    | ByName Text
    deriving (Show, Eq, Generic)

instance FromHttpApiData TagQuery

data SearchResults = SearchResults
    { searchResultsGifts :: [GiftInfo]
    } deriving (Show, Eq, Generic)

instance FromJSON SearchResults where
    parseJSON =
        withObject "SearchResults" $ \o -> SearchResults <$> o .: "gifts"

instance ToJSON SearchResults where
    toJSON SearchResults {..} = object ["gifts" .= searchResultsGifts]

data GiftInfo = GiftInfo
    { giftInfoName :: Text
    , giftInfoLink :: Text
    , giftInfoMatches :: [MatchInfo]
    } deriving (Show, Eq, Generic)

instance FromJSON GiftInfo where
    parseJSON =
        withObject "GiftInfo" $ \o ->
            GiftInfo <$> o .: "name" <*> o .: "link" <*> o .: "matches"

instance ToJSON GiftInfo where
    toJSON GiftInfo {..} =
        object
            [ "name" .= giftInfoName
            , "link" .= giftInfoLink
            , "matches" .= giftInfoMatches
            ]

data MatchInfo = MatchInfo
    { matchInfoTag :: TagInfo
    , matchInfoTag2 :: Maybe TagInfo
    } deriving (Show, Eq, Generic)

instance FromJSON MatchInfo where
    parseJSON =
        withObject "MatchInfo" $ \o -> MatchInfo <$> o .: "tag" <*> o .: "tag2"

instance ToJSON MatchInfo where
    toJSON MatchInfo {..} =
        object ["tag" .= matchInfoTag, "tag2" .= matchInfoTag2]

data TagInfo = TagInfo
    { tagInfoName :: Text
    , tagInfoColor :: Text
    } deriving (Show, Eq, Generic)

instance FromJSON TagInfo where
    parseJSON =
        withObject "TagInfo" $ \o -> TagInfo <$> o .: "name" <*> o .: "color"

instance ToJSON TagInfo where
    toJSON TagInfo {..} =
        object ["name" .= tagInfoName, "color" .= tagInfoColor]
