import (import ./nix/nixpkgs.nix) {
  overlays = [ (import ./nix/overlay.nix) (import ./nix/front-overlay.nix) ];
  config.allowUnfree = true;
}
