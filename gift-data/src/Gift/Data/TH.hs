{-# LANGUAGE TemplateHaskell #-}

module Gift.Data.TH
    ( allModels
    ) where

import Import

import System.IO (readFile)

import Language.Haskell.TH
import Language.Haskell.TH.Quote
import Language.Haskell.TH.Syntax

import Database.Persist.Quasi
import Database.Persist.TH

allModels :: Q Exp
allModels = do
    fs <- runIO $ snd <$> listDirRecur $(mkRelDir "models")
    mapM_ (qAddDependentFile . toFilePath) fs
    contents <-
        runIO $
        unlines <$> mapM (readFile . toFilePath) (filter (not . hidden) fs)
    quoteExp (persistWith lowerCaseSettings) contents

-- TODO filter all hidden files, not just vim swp files
hidden :: Path Abs File -> Bool
hidden f = ".swp" `isSuffixOf` fileExtension f
