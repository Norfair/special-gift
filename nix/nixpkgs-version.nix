# To update:
# nix-prefetch-url --unpack https://github.com/NixOS/nixpkgs/archive/07723f14bb132f6d65c74b953bef39cd80457273.tar.gz

{
  rev    = "bf9ad8da0749e59bffa3160488ae3ccadb0216a9";
  sha256 = "1jzyd8726n9hrvwcyllg6hh52carb09nhnjyv8drqrz71igvkdcs";
}
