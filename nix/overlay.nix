final:
  previous:
    with final.haskell.lib;
    {
      giftPackages =
            let pathFor = name:
                  builtins.path {
                      inherit name;
                      path = ../. + "/${name}";
                      filter = path: type:
                        !(final.lib.hasPrefix "." (baseNameOf path));
                    };
                giftPkg = name:
                overrideCabal (failOnAllWarnings (disableLibraryProfiling (final.haskellPackages.callCabal2nix name (pathFor name) {})))
                (old: {
                  preBuild =
                    ''
                    export FRONT_CODE=${final.gift-front}/Main.js
                    '';
                  }
                );
            in final.lib.genAttrs [
              "gift"
              "gift-data"
              "gift-api"
              "gift-server"
            ] giftPkg;

      haskellPackages = previous.haskellPackages.override (old: {
        overrides = final.lib.composeExtensions (old.overrides or (_: _: {})) (
          self: super: final.giftPackages
        );
      });
    }
