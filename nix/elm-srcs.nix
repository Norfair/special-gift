{ fetchzip }: {
  "elm-lang/core" = {
    src = fetchzip {
      url = "https://github.com/elm-lang/core/archive/5.1.1.zip";
      sha256 = "0iww5kfxwymwj1748q0i629vyr1yjdqsx1fvymra6866gr2m3n19";
    };
    version = "5.1.1";
  };
  "elm-lang/html" = {
    src = fetchzip {
      url = "https://github.com/elm-lang/html/archive/2.0.0.zip";
      sha256 = "08mxkcb1548fcvc1p7r3g1ycrdwmfkyma24jf72238lf7j4ps4ng";
    };
    version = "2.0.0";
  };
  "elm-lang/http" = {
    src = fetchzip {
      url = "https://github.com/elm-lang/http/archive/1.0.0.zip";
      sha256 = "1bhhh81ih7psvs324i5cdac5w79nc9jmykm76cmy6awfp7zhqb4b";
    };
    version = "1.0.0";
  };
  "elm-lang/virtual-dom" = {
    src = fetchzip {
      url = "https://github.com/elm-lang/virtual-dom/archive/2.0.4.zip";
      sha256 = "1zydzzhpxivpzddnxjsjlwc18kaya1l4f8zsgs22wkq25izkin33";
    };
    version = "2.0.4";
  };
}
