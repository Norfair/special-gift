final:
  previous: 
    {
      gift-front = 
        let
          mkDerivation =
            { srcs ? ./elm-srcs.nix
            , src
            , name
            , srcdir ? "./src"
            , targets ? []
            }:
            let
              sources = import srcs { fetchzip = final.fetchzip; };
              exactDependencies = builtins.toFile "exact-dependencies.json"
                (builtins.toJSON (final.lib.mapAttrs (name: value: value.version) sources));
            in final.stdenv.mkDerivation {


              inherit name src;

              buildInputs = [ final.elmPackages.elm ];

              preConfigurePhases = ["setupElmStuffPhase"];

              setupElmStuffPhase = ''
                runHook preSetupElmStuffPhase

                rm -rf elm-stuff
                mkdir elm-stuff
                ln -s ${exactDependencies} elm-stuff/exact-dependencies.json

                ${final.lib.concatStrings (final.lib.mapAttrsToList (name: src: ''
                  mkdir -p elm-stuff/packages/${name}
                  ln -s ${src.src} elm-stuff/packages/${name}/${src.version}
                '') sources)}

                runHook postSetupElmStuffPhase
              '';

              buildPhase = let
                elmfile = module: "${srcdir}/${builtins.replaceStrings ["."] ["/"] module}.elm";
              in ''
                mkdir -p $out/share/doc
                export HOME=.
                ${final.lib.concatStrings (map (module: ''
                  elm make ${elmfile module} --output $out/${module}.js --docs $out/share/doc/${module}.json
                '') targets)}
              '';

              installPhase = ":";
            };
        in mkDerivation {
          name = "gift-front";
          srcs = ./elm-srcs.nix;
          src = ../gift-front;
          srcdir = "src";
          targets = ["Main"];
        };
    }
